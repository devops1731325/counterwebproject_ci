package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class AllTest {


    @Test
    public void testAdd() throws Exception {
        //calling add method
        int k= new Calculator().add();
        assertEquals("Add", 9, k);
        
    }
    @Test
    public void testMul() throws Exception {

        int k= new Calcmul().mul();
        assertEquals("Mul", 18, k);

    }

    @Test
    public void testCalDeviderPositive() throws Exception {

        int k = new CalDevider().devide(10,2);
         assertEquals("devide", 5, k);
    }


    @Test
    public void testDeviderNegative() throws Exception {

        int k = new CalDevider().devide(10,0);
         assertEquals("d-negative", Integer.MAX_VALUE, k);
    }

       @Test
    public void testAbout() throws Exception {

        String k = new About().desc();
         assertEquals("About Test", "This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!", k);
    }

}